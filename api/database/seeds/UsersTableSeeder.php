<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = DB::table('users')->count();
        if($check == 0) {
            DB::table('users')->insert([
                'email' => 'mokey@monkey.vn',
                'password' => bcrypt('123123123'),
            ]);
        }
    }
}
