import {Injectable} from '@angular/core';
import {User, Token} from '../const/user';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {Const} from '../const';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(
        private http: HttpClient,
        private router: Router,
    ) {
    }

    login(user: User): Observable<Token> {
        return this.http.post<Token>(Const.baseAPI + '/login', user);
    }

    getAuth(token: Token) {
        return this.http.post(Const.baseAPI + '/test', token).pipe(
            catchError(error => {
                this.router.navigate(['/login']);
                return throwError('Not Logged In');
            })
        );
    }
}