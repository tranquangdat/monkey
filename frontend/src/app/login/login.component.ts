import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../services/user.service';
import {User} from '../const/user';
import {Router} from "@angular/router";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    submitted = false;

    constructor(
        private login: FormBuilder,
        private Users: UserService,
        private router: Router,
    ) {
    }

    ngOnInit() {
        this.loginForm = this.login.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(8)]],
        });
    }

    get validate() {
        return this.loginForm.controls;
    }

    submit() {
        this.submitted = true;
        if (this.loginForm.invalid) {
            return;
        }
        this.Users.login(this.loginForm.value)
            .subscribe(result => {
                localStorage.setItem('tokenMonkey', result.token);
                this.router.navigate(['/manage']);
            });
    }
}
