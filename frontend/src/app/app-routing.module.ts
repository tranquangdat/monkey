import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {ManagementComponent} from './management/management.component';
import {CategoryComponent} from './category/category.component';
import {ProductComponent} from './product/product.component';

const routes: Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'manage', component: ManagementComponent,
        children: [
            { path: '', redirectTo: 'category', pathMatch: 'full' },
            { path: 'category', component: CategoryComponent },
            { path: 'product', component: ProductComponent }
        ]
    }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  declarations: []
})
export class AppRoutingModule { }
