import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {UserService} from './services/user.service';
import {Token} from './const/user';
import {Router} from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'app';
    token: Token = { token : localStorage.getItem('tokenMonkey')};

    constructor(
        private Users: UserService,
        private router: Router,
    ) {
    }

    ngOnInit() {
        this.Users.getAuth(this.token).subscribe(result => {
            this.router.navigate(['/manage']);
        });
    }
}
